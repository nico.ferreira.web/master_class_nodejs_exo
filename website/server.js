var express = require('express');
var app = express();
var exphbs = require('express-handlebars');// moteur de template
const bodyParser = require('body-parser') // To receive POST values in Express, you first need to include the body-parser middleware, which exposes submitted form values on req.body in your route handlers

const FilmService = require('./services/FilmService')

// init handlebars
app.engine('handlebars', exphbs({defaultLayout: 'main'}));// on définit la view "main" comme étant la view de base du template
app.set('view engine', 'handlebars');// Moteur de template

app.use(express.static('public')); // init du fichier static pour le css et les images

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

///////// routing ///////////

// CLIENT PAGES 
app.get('/', function(req, res){
  FilmService.findAll(function(datas){
    res.render('pages/index', {
      datas: datas.films
      });
  })   
});

app.get('/films/:_id', function(req, res){
  let id = req.params._id
  FilmService.findOne(id, function(film){
    res.render('pages/film', {
       datas : film
      });
  })
});


// 404 page
app.use(function(req, res, next){
    res.status(404);
    // respond with html page
    if (req.accepts('html')) {
      res.render('pages/404', { url: req.url });
      return;
    }
});


var port = 3000;
app.listen(port);
console.log(`Cinefilms listening on port ${port} `)