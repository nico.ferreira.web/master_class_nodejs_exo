const fetch = require('node-fetch');
const urlApi = 'http://localhost:3001/api/films' 

module.exports = {
    // requete qui envoie un nouveau film dans la bdd , requete api
    async create(body,callback){
       
    },

    // requete qui retourne la liste des toutes les films de la bdd , requete api
    async findAll(callback){
        let response = await fetch(urlApi)
        let datas = await response.json()
        callback(datas) 
    },

    // requete qui retourne les données d'un film de la bdd avec son id , requete api
    async findOne(id, callback){
        let response = await fetch(`${urlApi}/${id}`)
        let datas = await response.json()
        callback(datas) 
    },

    // requete qui modifie les données d'un film dans la bdd avec son id  , requete api
    async update(id,body,callback){

    },

    // requete qui supprimer les données d'un film dans la bdd avec son id , requete api
    async delete(id,callback){

    },

}