# Master class - Nodejs - Mars 2019 


Nécessite Node.js (v: 10+) , et MongoDB (v: 3.6+).
 
## Partie 1 : création d'une API 

### Installation
Installation des dépendances et devDependencies puis démarrer l'api.

```sh
$ cd api
$ npm install
$ npm run dev
```

### Instructions

##### Création du modèle ( mongoose ):
- titre
- date de réalisation
- chemin image film
- description
- chemin poster film ( plus grosse image , option )

#####  Création du controlleur lié au modèle

Libre à vous de changer les méthodes du fichier de base, mais il est recommandé de ce servir de celles ci.

#####  Création des routes lié au controlleur

Suivre l'exemple pour écrire toutes les routes de notre API.

#####  Phase de test avec Postman :

Installer Postman , cet outil vous permet de faire des tests de votre API, vous pouvez tester toutes requetes : POST , GET , UPDATE, DELETE.

A ce stade votre API fonctionne ! 

Maintenant on va se servir de cette API sur un site web développé aussi avec nodejs.

## Partie 2 : création du site web


### Installation


Installation des dépendances et devDependencies puis démarrer l'application ( website et API ).

```sh
$ cd website
$ npm install
$ npm run dev
```

Vous disposez d'une base de projet, le but de l'excercice est de réaliser une vidéothèque, un site web stockant les informations sur des films. Ce site doit permettre de visualiser sur une page Home tout les films, ainsi qu'une page pour chaque film avec les détails de celui ci.

Le site doit aussi comporté une partie administration, avec gestion du CRUD d'un film.

Pages admin : index/create/edit/delete

De base dans ce projet le moteur de template "Handlebars" est intégré mais libre à vous d'en utilisé un autre : Pug , Ejs ...



