//Set up mongoose connection
const mongoose = require('mongoose');
const mongoDB = 'mongodb://localhost:27017/cinefilms';
mongoose.connect(mongoDB,{ useNewUrlParser: true });
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;

//Film Model
var FilmSchema = new Schema({

// Ici définir les différents champs de votre modèle

});

var Film = mongoose.model("Film", FilmSchema);
module.exports = Film;