const Film = require('../models/FilmModel');


module.exports = {
  // Create and Save a new Film
  create: function(req, res) {
    
  },
  
  // Retrieve and return all film from the database.
  findAll: function(req, res) {
    
  },
  
  // Find a single film with a filmId
  findOne: function(req, res) {
    
  },
  
  // Update a film identified by the filmId in the request
  update: function(req, res) {
    
  },
  
  // Delete a film with the specified filmId in the request
  delete: function(req, res) {
    
  },
  
}
