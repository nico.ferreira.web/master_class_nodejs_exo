const express = require('express')
const app = express()

const bodyParser = require('body-parser');
app.use(bodyParser.json())

// welcome pages
app.get('/api', function (req, res) {
    res.json({"welcome" : "Cinefilms REST API "});
})

app.listen(3001, function () {
  console.log('API listening on port 3001!')
})