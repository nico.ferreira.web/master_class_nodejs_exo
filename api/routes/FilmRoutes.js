const express = require('express');
const router = express.Router();
const FilmController = require('../controllers/FilmController');

// Production Film Routes
// exemple : router.get('/path', Controller.method);
    router.post();
    router.get('/films', FilmController.findAll);
    router.get();
    router.put();
    router.delete();

module.exports = router;